//
//  ViewController.swift
//  Json
//
//  Created by Atakan Cengiz KURT on 15.03.2017.
//  Copyright © 2017 Atakan Cengiz KURT. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

     var baslik = [String]()
    
    @IBOutlet weak var tblView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
       
        
        let urlString = "http://bucayapimarket.com/json.php"
        let url = URL(string: urlString)
        
        let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if error != nil {//Hata varsa
            print(error!)
            }else{
                do{
                    let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSArray
                    
                    if let jsonDic = json{
                        for i in 0..<jsonDic.count{
                            if let basliklar = jsonDic[i] as? NSDictionary{

                            self.baslik.append(basliklar["baslik"] as? NSString as! String)
                                
//                          print(basliklar["baslik"] as? NSString ?? "Başlık bulunamadı")
                                
                            }
                        }
                        print(self.baslik)
                    }
//                    for i in 0...5{
//                    let jsonDic = json[i] as! NSDictionary
//                    print(jsonDic["baslik"] as! NSString)
//                    }
                    self.tblView.reloadData()
                }
                catch{
                print(error)
                }
            }
        }
        task.resume()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //Tableview Methodları
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
    return 3
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
    let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! CampainTableViewCell
        cell.campainName.text = "Deneme"
        cell.remainTime.text = "3 gün kaldı"
        cell.productImage.image = UIImage(named: "tv")
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
}

