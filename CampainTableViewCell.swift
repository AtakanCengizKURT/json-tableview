//
//  CampainTableViewCell.swift
//  Json
//
//  Created by Atakan Cengiz KURT on 23.03.2017.
//  Copyright © 2017 Atakan Cengiz KURT. All rights reserved.
//

import UIKit

class CampainTableViewCell: UITableViewCell {

    @IBOutlet weak var cellBgImage: UIImageView!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var campainName: UILabel!
    @IBOutlet weak var remainTime: UILabel!
   
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
